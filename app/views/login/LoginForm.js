import React,{Component} from 'react';
import { View, Text,StyleSheet,Button, Platform, Dimensions, TouchableOpacity } from 'react-native';

import Input from '../../components/input';
import ValidationRules from '../../utils/forms/validationRules';

import { connect } from 'react-redux';
import { signUp, signIn} from '../../store/actions/User_Actions';
import { bindActionCreators } from 'redux';
 

import { setTokens } from '../../utils/misc';

class LoginForm extends Component {

    state = {
        type:'Login',
        action:'Login',
        actionMode:'I want to register',
        hasErrors:false,
        form:{
            email:{
                value:"",
                valid:false,
                type:"textinput",
                rules:{
                    isRequired:true,
                    isEmail:true
                }
            },
            password:{
                value:"",
                valid:false,
                type:"textinput",
                rules:{
                    isRequired:true,
                    minLength:6
                }
            },
            confirmPassword:{
                value:"",
                valid:false,
                type:"textinput",
                rules:{
                    confirmPass:'password'
                }
            }
        }
    }

    formHasErrors = () => (
        this.state.hasErrors ?
            <View style={styles.errorContainer}>
                <Text style={styles.errorLabel}>Oops, check your info.</Text>
            </View>
        :null
    )

    confirmPassword = () => (
        this.state.type != 'Login' ? 
            <Input
                    placeholder="Confirm your password"
                    placeholderTextColor="#cecece"
                    type={this.state.form.confirmPassword.type}
                    value={this.state.form.confirmPassword.value}
                    onChangeText={ value => this.updateInput("confirmPassword",value)}
                    //overrideStyle={{}}
                    secureTextEntry
            />
        :null
    )

    changeFormType = () => {
        const type = this.state.type;

        this.setState({
            type: type === 'Login' ? 'Register':'Login',
            action: type === 'Login' ? 'Register':'Login',
            actionMode: type === 'Login' ? 'I want to Login':'I want to register'
        })

    }
        

    updateInput = (name, value) => {
        this.setState({
            hasErrors:false
        });

        let formCopy = this.state.form;
        formCopy[name].value = value;

        ///rules
        let rules = formCopy[name].rules;
        let valid = ValidationRules(value,rules,formCopy);

        formCopy[name].valid = valid;

        this.setState({
            form:formCopy
        })
    }

    manageAccess = () => {
        if(!this.props.User.auth.uid){
            this.setState({hasErrors:true})
        } else {
            setTokens(this.props.User.auth,()=>{
                this.setState({hasErrors:false});
                this.props.goNext();
            })
        }
    }

    submitUser = () => {
        let isFormValid = true;
        let formToSubmit = {};
        const formCopy = this.state.form;

        for(let key in formCopy){
            if(this.state.type === 'Login'){
                //LOGIN
                if(key !== 'confirmPassword'){
                    isFormValid = isFormValid && formCopy[key].valid;
                    formToSubmit[key] = formCopy[key].value;
                }

            } else{
                // REGISTER
                isFormValid = isFormValid && formCopy[key].valid;
                formToSubmit[key] = formCopy[key].value;
             }
        }

        if(isFormValid){
            if(this.state.type === 'Login'){
                this.props.signIn(formToSubmit).then(()=>{
                    this.manageAccess()
                })
            } else {
                this.props.signUp(formToSubmit).then(()=>{
                    this.manageAccess()
                })
            }
        } else {
            this.setState({
                hasErrors:true
            })
        }

    }


    render(){
        return (
            <View style={styles.inputWrapper}>
              
                <Input
                    placeholder="Enter email"
                    placeholderTextColor="#cecece"
                    type={this.state.form.email.type}
                    value={this.state.form.email.value}
                    autoCapitalize={"none"}
                    keyboardType={"email-address"}
                    onChangeText={ value => this.updateInput("email",value)}
                    //overrideStyle={{}}
                />

                <Input
                    placeholder="Enter your password"
                    placeholderTextColor="#cecece"
                    type={this.state.form.password.type}
                    value={this.state.form.password.value}
                    onChangeText={ value => this.updateInput("password",value)}
                    //overrideStyle={{}}
                    secureTextEntry
                />

                {this.confirmPassword()}
                {this.formHasErrors()}
                <View style={styles.buttonWrapper}>
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.buttonText}>LOGIN</Text>
                        </TouchableOpacity>
                </View>
                <View style={ styles.horizontalLineWrapper}>

               
                <View style={styles.hairline} />
                    <Text style={styles.loginButtonBelowText1}>Or</Text>
                <View style={styles.hairline} />
                </View>
                <View style={styles.buttonWrapper}>
                        <TouchableOpacity style={[styles.button, styles.createAccountButton]}>
                            <Text style={styles.createAccountButtonText}>Create Account</Text>
                        </TouchableOpacity>
                </View>
                <View style={ styles.forgotPwdWrapper }>
                    <View style={styles.forgotPwd}>
                    <TouchableOpacity>
                            <Text style={styles.forgotPwdText}>Forgot Your Password?</Text>
                    </TouchableOpacity>
                        
                    </View>
                </View>
                

            </View>
           
        )
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    errorContainer:{
        marginBottom:10,
        marginTop:30,
        padding:10,
        backgroundColor:'#f44336'
    },
    errorLabel:{
        color:'#fff',
        textAlignVertical:'center',
        textAlign:'center'
    },
    button:{
      ...Platform.select({
          ios:{
              marginBottom:0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#f63',
              height: 40,
              width: DEVICE_WIDTH - 80,
              marginTop:10,
              borderRadius:5,
              color:'white',
             
          },
          android:{
              marginBottom:10,
              marginTop:10,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#f63',
              height: 40,
              width: DEVICE_WIDTH - 80,
              borderRadius:5,
              color:'white',
          }
      })
    },
    inputWrapper: {
        display:"flex",
        justifyContent:"center",
        alignItems:"center"
    },
    buttonWrapper:{
        display:"flex"
    },
    buttonText:{
        color: "white"
    },
    createAccountButton:{
        backgroundColor:"white",
    },
    buttonText:{
        color: "white"
    },
    createAccountButtonText:{
        color: "#f63"
    },
    forgotPwdWrapper:{
        marginTop:40
    },
    forgotPwdText:{
        color:"#fff"
    },
    forgotPwd:{
        ...Platform.select({
            ios:{
                marginBottom:0,
            },
            android:{
                marginBottom:10,
                marginTop:10,
            }
        })
    },
    hairline: {
        backgroundColor: 'white',
        height: 1,
        width: 130
    },
      
    loginButtonBelowText1: {
        fontFamily: 'AvenirNext-Bold',
        fontSize: 14,
        paddingHorizontal: 5,
        alignSelf: 'center',
        color: 'white',
        position:"relative",
        top: -10
    },
    horizontalLineWrapper:{
        flexDirection:"row",
        marginTop:30
    }
})


function mapStateToProps(state){
    console.log(state)
    return {
        User: state.User
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({signIn,signUp},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(LoginForm);