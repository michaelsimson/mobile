import React, {Component} from 'react';
import {StyleSheet, View, Text,ScrollView,ActivityIndicator, ImageBackground, Dimensions} from 'react-native';

import LoginLogo from './LoginLogo';
import LoginForm from './LoginForm';

import { connect } from 'react-redux';
import { autoSignIn } from '../../store/actions/User_Actions';
import { bindActionCreators } from 'redux';
 
import { getTokens , setTokens } from '../../utils/misc';
import backgroundImage from "../../assets/images/apartmentCabinetContemporary1027508.png"



class LoginScreen extends Component {

  state = {
    loading:true
  }


  goNext = () => {
    this.props.navigation.navigate('App')
  }

  componentDidMount(){
    getTokens((value)=>{
      if(value[0][1]===null){
        this.setState({loading:false})
      } else{
        this.props.autoSignIn(value[1][1]).then(()=>{
          if(!this.props.User.auth.token){
            this.setState({loading:false})
          }else{
            setTokens(this.props.User.auth,()=>{
              this.goNext();
            })
          }
        })
      }
    })
  }


  render() {
    if(this.state.loading){
      return (
        <View style={styles.loading}>
          <ActivityIndicator/>
        </View>
      )
    } else {
      return (
       <ScrollView style={styles.scrollview}>
         <ImageBackground style={[styles.container, styles.fixed, {zIndex:-1} ]} source={ backgroundImage }>
        <View style={ styles.logoWrapper }>
          <LoginLogo/>
         
        </View>
        <View>
        <LoginForm
            goNext={this.goNext}
          />
        </View>
        </ImageBackground>
       </ScrollView>
      );
    }
    
  }
}

const styles = StyleSheet.create({
  container:{
    width: Dimensions.get("window").width, //for full screen
    height: Dimensions.get("window").height //for full screen
  },
  loading:{
    flex:1,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center'
  },
  picture:{
    justifyContent:"center",
    alignItems:"center",
    resizeMode:"cover",
    height:"100%",
    width:"100%"
  },
  fixed: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  scrollview: {
   backgroundColor: 'transparent'
  },
  logoWrapper:{
    display:"flex",
    marginTop:120
  }
});


function mapStateToProps(state){
  return {
      User: state.User
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({autoSignIn},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen);