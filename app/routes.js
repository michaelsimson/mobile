import React from 'react';
import { Platform } from "react-native";
import {
    createStackNavigator,
    createAppContainer,
    createSwitchNavigator
} from 'react-navigation';

// SCREENS
import SignIn from './views/login/LoginScreen';


const AuthStack = createStackNavigator({
    SignIn:SignIn
},{
    headerMode: 'none'
});

export const RootNavigator = () => {
    return createAppContainer(createSwitchNavigator({
        Auth:AuthStack
    },{
        initialRouteName:'Auth'
    }))
}

